import 'package:shared_pref/model/user.dart';

class MyDatabase {

  static List<MyUser> users = [
    MyUser(id: 0, name: "First user", email: "sally@gmail.com", password: "1234567"),
    MyUser(id: 1, name: "Second user", email: "user2@gmail.com", password: "abc1234"),
    MyUser(id: 2, name: "Third user", email: "user3@gmail.com", password: "def0987"),
  ];

}