import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_pref/pages/auth/login.dart';
import 'package:shared_pref/pages/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  runApp(MyApp());
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();

  print("Handling a background message: ${message.messageId}");
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white38,
        primarySwatch: Colors.blue,
        accentColor: Colors.orange,
        primaryColor: Colors.white,
        textTheme: TextTheme(
          subtitle1: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
          subtitle2: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
          bodyText1: TextStyle(fontSize: 14, fontWeight: FontWeight.normal, color: Colors.white),
          bodyText2: TextStyle(fontSize: 12, fontWeight: FontWeight.normal, color: Colors.white),
          caption: TextStyle(fontSize: 10, fontWeight: FontWeight.normal, color: Colors.white),
        )
      ),
      home: SplashScreen(),
    );
  }
}
