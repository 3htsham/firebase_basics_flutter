import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_pref/model/chat_message.dart';
import 'package:shared_pref/model/user.dart';

class ChatScreen extends StatefulWidget {

  MyUser receiver;

  ChatScreen({this.receiver});

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {

  ChatMessage message = ChatMessage();

  List<ChatMessage> messages = <ChatMessage>[];

  User currentUser = FirebaseAuth.instance.currentUser;
  var documentId;

  TextEditingController messageController = TextEditingController();

  @override
  initState() {
    super.initState();
    checkDocumentAndListenChanges();
  }


  sendMessage() async {
    if(messageController.text != null && messageController.text.length > 0) {
      await checkDocument();

      ///Now, check if documentID is still null or not
      if (documentId == null) {
        await createDocument();
      }

      DateTime currentPhoneDate = DateTime.now(); //DateTime
      Timestamp myTimeStamp = Timestamp.fromDate(currentPhoneDate);

      ///Document exists now
      message.message = messageController.text;
      message.time = myTimeStamp;
      message.senderID = this.currentUser.uid;


      var ref = FirebaseFirestore.instance.collection("chats");
      var document = ref.doc(documentId);
      var msgRef = document.collection("messages");

      msgRef.add(message.toJson()).then((value) =>  print("New message added"));
      messageController.clear();

    }

  }

  checkDocument() async {
    var ref = FirebaseFirestore.instance.collection("chats");

    ///Check Document
    /// - Document ID? => user1ID_user2ID OR user2ID_user1ID
    /// - Add users Array if not exists [user1ID, user2ID]


    ///=> If Document 1 exists
    //senderID_receiverID
    var docId1 = '${currentUser.uid}_${widget.receiver.id}';
    var doc1 = await ref.doc(docId1).get();
    if(doc1.exists) {
      documentId = docId1;
      return;
    }


    ///=> If Document 2 exists
    //receiverID _senderID
    var docId2 = '${widget.receiver.id}_${currentUser.uid}';
    var doc2 = await ref.doc(docId2).get();

    if(doc2.exists) {
      documentId = docId2;
      return;
    }

    ///If none of them exists
    ///Create new Document

  }

  createDocument() async {
    var ref = FirebaseFirestore.instance.collection("chats");
    var docId1 = '${currentUser.uid}_${widget.receiver.id}';
    var docRef = ref.doc(docId1);

    await docRef.set({
      "users": [currentUser.uid, widget.receiver.id]
    }).then((value) {
      documentId = docId1;
      print("New Document created");
    })
    .catchError((e) => print("Error creating new document $e"));
  }

  listenMessages() async {
    var ref = FirebaseFirestore.instance.collection("chats");
    var document = ref.doc(documentId);
    var msgRef = document.collection("messages").orderBy('time', descending: false);

    var snapshots = msgRef.snapshots();
    snapshots.listen((_snapshot) {
      var docs = _snapshot.docs;

      this.messages.clear();

      if(docs.isNotEmpty) {
        ///Add all documents to messages
        for(int i=0; i<docs.length; i++) {
          var singleDoc = docs[i].data();

          ChatMessage msg = ChatMessage();
          msg.message = singleDoc['message'];

          var timeStamp = singleDoc['time'] as Timestamp;

          msg.time = timeStamp.toDate();
          msg.senderID = singleDoc['senderID'];

          setState((){
            this.messages.add(msg);
          });

        }
      }

    },
      onError: (e) {
      print("Some error occured in stream $e");
      }
    );

  }


  checkDocumentAndListenChanges() async {
    await checkDocument();
    ///Now, check if documentID is still null or not
    if (documentId == null) {
      await createDocument();
    }
    listenMessages();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        title: Text(
          widget.receiver?.name ?? "Chat",
          style: Theme.of(context).textTheme.subtitle2,
        ),
        leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Colors.white,),
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: [

            Expanded(child: Container(
              child: Column(
                children: [

                  this.messages.isEmpty
                      ? SizedBox()
                      : ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: this.messages.length,
                    itemBuilder: (context, i) {

                      bool isCurrentUser = this.currentUser.uid == this.messages[i].senderID;

                      return Row(
                        mainAxisAlignment: isCurrentUser ? MainAxisAlignment.end : MainAxisAlignment.start,
                        children: [
                          Container(
                            color: Colors.blue,
                            margin: EdgeInsets.symmetric(vertical: 5),
                            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                            width: MediaQuery.of(context).size.width / 2,
                            child: Text(this.messages[i].message),
                          ),
                        ],
                      );
                    },
                  )

                ],
              ),
            )),

            Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      keyboardType: TextInputType.emailAddress,
                      style: Theme.of(context).textTheme.bodyText1,
                      controller: messageController,
                      decoration: InputDecoration(
                        hintText: "Type you message",
                        hintStyle: Theme.of(context).textTheme.bodyText1,
                        border: InputBorder.none
                      ),
                    ),
                  ),
                  IconButton(
                    onPressed: (){
                      sendMessage();
                    },
                    icon: Icon(Icons.send, color: Colors.green,)
                  )
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
