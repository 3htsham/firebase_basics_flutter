import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_pref/model/bus_route.dart';
import 'package:location/location.dart' as l;
import 'package:shared_pref/model/distance_model.dart';

class StopSelection extends StatefulWidget {
  BusRoute selectedRoute;
  StopSelection({Key key, this.selectedRoute}) : super(key: key);

  @override
  _StopSelectionState createState() => _StopSelectionState();
}

class _StopSelectionState extends State<StopSelection> {

  //TODO: Calculate Distance between routes
  DistanceModel nearestRouteDistance;
  List<DistanceModel> distances = [];
  List<BusRoute> _routes = [];
  BusRoute _previousRoute;
  BusRoute _selectedRoute;
  bool isLoading = false;

  @override
  initState() {
    this._selectedRoute = widget.selectedRoute;
    this._previousRoute = widget.selectedRoute;
    super.initState();
    _getRoutes();
  }

  _getRoutes() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference routesRef = firestore.collection('routes');
    routesRef.get().then((snapshot) {
      if(snapshot.docs.isNotEmpty) {
        final docs = snapshot.docs;
        docs.forEach((docSnap) {
          final _route = BusRoute.fromJson(docSnap.data());
          setState(() {
            _routes.add(_route);
          });
        });
        getUserLocation();
      }
    });
  }

  _updateRoute() async {
    if(this._selectedRoute != null) {
      setState(() {
        this.isLoading = true;
      });
      try {
        final _authUser = FirebaseAuth.instance.currentUser;
        FirebaseFirestore firestore = FirebaseFirestore.instance;
        CollectionReference users = firestore.collection('users');
        users.doc(_authUser.uid).update({'stop': _selectedRoute.id});

        //Subscribe to FCM Topic
        FirebaseMessaging messaging = FirebaseMessaging.instance;
        try {
          messaging.unsubscribeFromTopic(this._previousRoute.id);
        } catch (e) {

        }
        messaging.subscribeToTopic(this._selectedRoute.id);

        setState(() {
          this.isLoading = false;
        });
        Navigator.of(context).pop(true);
      } catch (e) {
        print(e);
        setState(() {
          this.isLoading = false;
        });
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Please select a route")));
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;

    var nearestRoute;
    if(nearestRouteDistance != null) {
      nearestRoute = _routes.firstWhere((element) => element.id == nearestRouteDistance.routeId);
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Select your Route'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                SizedBox(height: 15,),

                nearestRouteDistance != null
                ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Text('Your nearest Route/Stop', style: textTheme.bodyText1,),

                    ListTile(
                      title: Text('${nearestRoute.name}', style: textTheme.bodyText2,),
                      subtitle: Text('${nearestRouteDistance.distance.toStringAsFixed(2)} KMs', style: textTheme.caption,),
                      trailing: myCheckbox(_selectedRoute != null && _selectedRoute.id == nearestRoute.id),
                      onTap: (){
                        if(_selectedRoute != null && _selectedRoute.id == nearestRoute.id) {
                          setState(() {
                            this._selectedRoute = null;
                          });
                        } else {
                          setState(() {
                            this._selectedRoute = nearestRoute;
                          });
                        }
                      },
                    ),

                    SizedBox(height: 15,),
                  ],
                )
                : SizedBox(),

                Text('Select bus routes from list below', style: textTheme.bodyText1,),
                SizedBox(height: 5),
                _routes.isEmpty
                    ? SizedBox()
                    : ListView.builder(
                  primary: false,
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: _routes.length,
                  itemBuilder: (context, index) {

                    final route = _routes[index];

                    final _isInSelected = _selectedRoute != null && _selectedRoute.id == route.id;
                    var _respectiveDistance = 0.0;

                    if(distances.length > 0) {
                      _respectiveDistance =
                          distances.firstWhere((element) => element.routeId ==
                              route.id).distance;
                    }

                    return ListTile(
                      title: Text('${route.name}', style: textTheme.bodyText2,),
                      subtitle: Text('${_respectiveDistance.toStringAsFixed(2)} KMs', style: textTheme.caption,),
                      trailing: myCheckbox(_isInSelected),
                      onTap: (){
                        if(_isInSelected) {
                          setState(() {
                            this._selectedRoute = null;
                          });
                        } else {
                          setState(() {
                            this._selectedRoute = route;
                          });
                        }
                      },
                    );
                  },
                ),

                SizedBox(
                  height: 15,
                ),

                this.isLoading
                    ? Center(child: SizedBox(height: 35, child: CircularProgressIndicator()))
                    : ElevatedButton(
                  onPressed: () {
                    _updateRoute();
                  },
                  style: ElevatedButton.styleFrom(shape: StadiumBorder()),
                  child: Center(
                    child: Text("Update"),
                  ),
                ),



              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget myCheckbox(bool isChecked) {
    return Container(
      height: 20,
      width: 20,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2),
          border: Border.all(color: Colors.green, width: 0.8),
          color: isChecked ? Colors.green : Colors.transparent
      ),
      child: Center(
        child: isChecked ? Icon(
          CupertinoIcons.checkmark_alt,
          color: Colors.white,
          size: 14,
        ) : SizedBox(),
      ),
    );
  }


  Future<bool> checkLocationPermission() async {
    var status = await l.Location.instance.hasPermission();
    if(status == l.PermissionStatus.granted) {
      var isService = await l.Location.instance.requestService();
      if(isService) {
        return true;
      } else {
        return false;
      }
    } else {
      status = await l.Location.instance.requestPermission();
      if(status == l.PermissionStatus.granted) {
        var isService = await l.Location.instance.requestService();
        return isService;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
                content: Text("You must allow permission to update location")
            )
        );
        //Navigator pop
        return false;
      }
    }
  }

  getUserLocation() async {
    if(await checkLocationPermission()) {
      final userLocation = await l.Location.instance.getLocation();
      // userLocation.latitude
      // userLocation.longitude

      for(int i = 0; i<this._routes.length; i++) {
        final _route = this._routes[i];
        final dist = calculateDistance(userLocation.latitude, userLocation.longitude,
            _route.location.latitude, _route.location.longitude);
        distances.add(DistanceModel(routeId: _route.id, distance: dist, routeName: _route.name));
      }

      distances.sort((a, b) => a.distance > b.distance ? 1 : 0);
      print('Sorted');
      if(distances != null && distances.length > 0) {
        print('nearest is  ${distances[0].routeName} ${distances[0].distance}');
        //Select Nearest Stop
        setState((){
          nearestRouteDistance = distances[0];
        });
      }

    } else {
      Navigator.of(context).pop();
    }
  }


  //lat1, long1 == User' Current Location
  //lat2, long2 == Stop location
  double calculateDistance(lat1, lon1, lat2, lon2){
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
            (1 - c((lon2 - lon1) * p))/2;
    return 12742 * asin(sqrt(a));
  }

}






