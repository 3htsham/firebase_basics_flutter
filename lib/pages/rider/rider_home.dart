import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_pref/global_variables.dart';
import 'package:shared_pref/model/bus.dart';
import 'package:shared_pref/model/bus_route.dart';
import 'package:shared_pref/model/user.dart';
import 'package:shared_pref/pages/auth/login.dart';
import 'package:shared_pref/pages/rider/bus_map.dart';
import 'package:shared_pref/pages/rider/stop_selection.dart';

class RiderHome extends StatefulWidget {
  MyUser currentUser;

  RiderHome({Key key, this.currentUser}) : super(key: key);

  @override
  _RiderHomeState createState() => _RiderHomeState();
}

class _RiderHomeState extends State<RiderHome> {
  MyUser currentUser;
  BusRoute _selectedRoute;
  List<Bus> buses = [];

  @override
  initState() {
    this.currentUser = widget.currentUser;
    super.initState();
    if(this.currentUser.stop != null) {
      //Get Stop then
      _getStopDetails(this.currentUser.stop);
    }
    subscribeNotificationsTopic();
  }

  _logout() async {
    ///Set prefs to loggedOut
    ///Clear the Activity Stack
    ///Navigate to Login page
    await FirebaseAuth.instance.signOut();

    //UnSubscribe to FCM Topic
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    try {
      messaging.unsubscribeFromTopic(this._selectedRoute.id);
      messaging.unsubscribeFromTopic(GlobalVariables.riderNotificationsTopic);
    } catch (e) {

    }

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
  }

  subscribeNotificationsTopic(){
    //Subscribe to FCM General Topic
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    messaging.subscribeToTopic(GlobalVariables.riderNotificationsTopic);
  }

  Future<MyUser> _getUserDetails() async {
    final _authUser = FirebaseAuth.instance.currentUser;
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference users = firestore.collection('users');
    final _userDetails = await users.doc(_authUser.uid).get();
    Map data = _userDetails.data();
    setState((){
      this.currentUser = MyUser.fromJson(data);
    });
    if(this.currentUser.stop != null) {
      //Get Stop then
      _getStopDetails(this.currentUser.stop);
    }
  }

  _getStopDetails(var stopId) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference routes = firestore.collection('routes');
    final _routeDetails = await routes.doc(stopId).get();
    Map data = _routeDetails.data();
    setState((){
      this._selectedRoute = BusRoute.fromJson(data);
    });

    //Subscribe to FCM Topic
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    messaging.subscribeToTopic(this._selectedRoute.id);

    _getMyRouteBuses();
  }

  _getMyRouteBuses() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference buses = firestore.collection('buses');
    this.buses.clear();
    final ref = buses.where('stops', arrayContains: this._selectedRoute.id);
    ref.get().then((snapshot) {
      if(snapshot.docs.isNotEmpty) {
        final docs = snapshot.docs;
        docs.forEach((docSnap) {
          final _bus = Bus.fromJson(docSnap.data());
          setState(() {
            this.buses.add(_bus);
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome ${this.currentUser?.name ?? ''}'),
        actions: [
          TextButton(
              onPressed: () {
                _logout();
              },
              child: Text(
                "Logout",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(color: Colors.black),
              ))
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              _selectedRoute == null
              ? _selectStopButton()
              : _stopIndicator(),

              Divider(thickness: 1,),

              this.buses.isEmpty
                  ? SizedBox()
              : Text(
                'Buses visiting your Stop',
                style: textTheme.subtitle2,
              ),
              SizedBox(height: 15,),
              this.buses.isEmpty
              ? SizedBox()
                  : ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: this.buses.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text('Bus No. (${this.buses[index].busNo})', style: textTheme.bodyText1,),
                    leading: Icon(CupertinoIcons.car_detailed, color: Colors.green,),
                    trailing: IconButton(
                      onPressed: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => BusMapPage(
                                  currentUser: this.currentUser,
                                  bus: this.buses[index],
                                )
                            )
                        );
                      },
                      icon: Icon(CupertinoIcons.location, color: Colors.redAccent,),
                    ),
                  );
                },
              )

            ],
          ),
        ),
      ),
    );
  }

  Widget _selectStopButton() {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;
    return InkWell(
      onTap: (){
        // StopSelection
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => StopSelection())).then((value) {
              if(value != null) {
                if(value) {
                  _getUserDetails();
                }
              }
        });
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(width: 0.5, color: Colors.white),
            borderRadius: BorderRadius.circular(3)
        ),
        padding: EdgeInsets.all(8),
        child: Row(
          children: [

            Text('Select your Stop', style: textTheme.bodyText1,),

            Spacer(),

            IconButton(icon: Icon(CupertinoIcons.forward, color: Colors.white,),
                onPressed: (){})

          ],
        ),
      ),
    );
  }

  Widget _stopIndicator() {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Your selected Stop', style: textTheme.bodyText1,),
        SizedBox(),
        Row(
          children: [
            Text('${this._selectedRoute.name ?? ''}', style: textTheme.subtitle2,),
            Spacer(),
            IconButton(icon: Icon(CupertinoIcons.pen, color: Colors.white,), onPressed: (){
              // StopSelection
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => StopSelection(selectedRoute: this._selectedRoute,))).then((value) {
                if(value != null) {
                  if(value) {
                    _getUserDetails();
                  }
                }
              });
            })
          ],
        ),
      ],
    );
  }

}
