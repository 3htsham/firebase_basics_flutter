import 'dart:async';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_pref/model/user.dart';
import 'package:shared_pref/pages/auth/login.dart';
import 'package:shared_pref/pages/driver/driver_home.dart';
import 'package:shared_pref/pages/rider/rider_home.dart';
import 'package:shared_pref/pages/welcome.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'main_pages/home.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {


  @override
  void initState() {
    setUpFCM();
    super.initState();
    loadData();
  }

  void loadData() async {
    Future.delayed(Duration(milliseconds: 700), () async {
      ///Get value and navigate
      final _dbUser = FirebaseAuth.instance.currentUser;
      bool isSignedIn = _dbUser != null;
      if(isSignedIn) {
        FirebaseFirestore firestore = FirebaseFirestore.instance;
        CollectionReference users = firestore.collection('users');
        final _userDetails = await users.doc(_dbUser.uid).get();
        Map data = _userDetails.data();
        final _user =  MyUser.fromJson(data);
        //Check if user is Driver or Rider
        if(_user.isDriver) {
          //TODO: Navigate to driver module
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => DriverHome(currentUser: _user,)));
        } else {
          //TODO: Navigate to Rider Modules
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => RiderHome(currentUser: _user,)));
        }
      } else {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => LoginPage()));
      }
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(CupertinoIcons.settings, color: Colors.white,),

            SizedBox(height: 20),


            SizedBox(
              height: 35,
              child: CircularProgressIndicator(),
            )
          ],
        ),
      ),
    );
  }


  setUpFCM() async {
    FirebaseMessaging messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('Got a message whilst in the foreground!');
      print('Message data: ${message.data}');

      if (message.notification != null) {
        print('Message also contained a notification: ${message.notification}');
        print(message.notification.title);
        print(message.notification.body);
        final notification  = message.notification;
        onMessage(notification);
      }

    });
  }

  void onMessage(RemoteNotification message) async {
    final androidSettings = AndroidInitializationSettings('noti_icon');
    final iosSettings = IOSInitializationSettings(onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final generalSetting = InitializationSettings(
      android: androidSettings,
      iOS: iosSettings
    );

    final androidChannel = AndroidNotificationChannel('high_importance_channel',
        'High importance channel',
        'This channel will be used for important notifications');

    final localNotificationPlugin = FlutterLocalNotificationsPlugin();
    localNotificationPlugin.initialize(generalSetting, onSelectNotification: null);

    await localNotificationPlugin
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(androidChannel);

    Random random = Random();
    final _id = random.nextInt(10000000);

    localNotificationPlugin.show(
        0,
        message.title,
        message.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
              androidChannel.id,
              androidChannel.name,
              androidChannel.description
          ),
          iOS: IOSNotificationDetails(
            badgeNumber: 0,
            presentAlert: true,
            presentSound: true,
            presentBadge: true
          )
        )
    );

  }

  Future<dynamic> onDidReceiveLocalNotification(
      int id, String title, String body, String payload) {

  }

}
