import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_pref/model/bus.dart';
import 'package:shared_pref/model/bus_route.dart';
import 'package:shared_pref/model/distance_model.dart';
import 'package:shared_pref/model/user.dart';
import 'package:location/location.dart' as l;
import 'dart:math' show cos, sqrt, asin;

import 'package:shared_pref/pages/driver/notification.dart';

class LocationTrackingPage extends StatefulWidget {
  MyUser currentUser;
  Bus bus;
  List<BusRoute> routes = [];

  LocationTrackingPage({Key key, this.currentUser, this.bus, this.routes}) : super(key: key);

  @override
  _LocationTrackingPageState createState() => _LocationTrackingPageState();
}

class _LocationTrackingPageState extends State<LocationTrackingPage> {
  MyUser currentUser;
  Bus bus;
  List<BusRoute> routes = [];
  bool isTracking = false;
  
  List<Marker> allMarkers = [];
  GoogleMapController _mapController;

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  void initState() {
    this.currentUser = widget.currentUser;
    this.bus = widget.bus;
    this.routes.addAll(widget.routes);

    super.initState();
    getLocationPermission();
  }

  @override
  Widget build(BuildContext context) {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    return Scaffold(
      key: this.scaffoldKey,
      body: Stack(
        children: [

          Container(
            width: size.width,
            height: size.height,
            // color: Colors.indigo,
            child: GoogleMap(
              onMapCreated: (GoogleMapController _controller) {
                this._mapController = _controller;
              },
              initialCameraPosition: CameraPosition(
                target: LatLng(33.6780513, 73.1843443), zoom: 13
              ),
              onTap: (post){},
              mapType: MapType.normal,
              markers: Set.from(this.allMarkers),
            ),
          ),


          this.isTracking
          ? SizedBox()
          : Positioned(
            bottom: 5,
            right: 10,
            left: 10,
            child: ElevatedButton(
              onPressed: () async {
                //TODO: Start location tracking
                if(await getLocationPermission()) {
                  setState((){
                    this.isTracking = true;
                  });
                  setLocationListener();
                }
              },
              style: ElevatedButton.styleFrom(shape: StadiumBorder()),
              child: Center(
                child: Text("Start"),
              ),
            ),
          ),

        ],
      ),
    );
  }


  Future<bool> getLocationPermission() async {
    if(await l.Location.instance.hasPermission() == l.PermissionStatus.granted) {
      ///Granted
      ///We can Update location
      return true;
    } else {
      final _req = await l.Location().requestPermission();
      if(_req == l.PermissionStatus.granted) {
        ///Granted
        ///We can Update location
        return true;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
              content: Text("You must allow permission to update location")
          )
        );
        return false;
      }
    }
  }

  setLocationListener() async {
    final listener = l.Location.instance.onLocationChanged;
        listener.listen((l.LocationData currentLocation) {
          if(mounted) {
            //GeoPoint
            GeoPoint geoPoint = GeoPoint(
                currentLocation.latitude, currentLocation.longitude);

            //Add Marker on Map
            addMarker(currentLocation);

            print("Bus Id: ${this.bus.id}");
            //Firebase Update
            FirebaseFirestore.instance
                .collection("buses")
                .doc(this.bus.id)
                .update({
              "currentLocation": geoPoint
            });

            //TODO: Calculate Distance between routes
            List<DistanceModel> distances = [];
            for(int i = 0; i<this.routes.length; i++) {
              final _route = this.routes[i];
              final dist = calculateDistance(currentLocation.latitude, currentLocation.longitude,
                  _route.location.latitude, _route.location.longitude);
              distances.add(DistanceModel(routeId: _route.id, distance: dist, routeName: _route.name));
            }
            distances.sort((a, b) => a.distance > b.distance ? 1 : 0);
            print('Sorted');
            if(distances != null && distances.length > 0) {
              print('nearest is  ${distances[0].routeName} ${distances[0].distance}');
              notifyUser(distances[0].routeId, distances[0].distance);
            }
          }
    });
  }

  notifyUser(String id, double distance) async {
    final dist = distance.toStringAsFixed(1);
    //TODO: Fire FCM
    await sendNotification(
        id,
        title: 'Be ready please',
        notificationBody: 'Bus #${this.bus.busNo} is $dist KMs away'
    );
  }

  addMarker(l.LocationData loc) {
    final _id = UniqueKey();
    final _marker = Marker(
      markerId: MarkerId(_id.toString()),
      draggable: false,
      infoWindow: InfoWindow(title: 'You are here', snippet: ''),
      position: LatLng(loc.latitude, loc.longitude)
    );
    setState(() {
      this.allMarkers.clear();
      this.allMarkers.add(_marker);
    });
    moveCamera(loc);
  }

  moveCamera(l.LocationData _loc) {
    _mapController.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
          target: LatLng(
              _loc.latitude,
            _loc.longitude
          ),
        zoom: 13,
      ),
    ),);
  }

  //lat1, long1 == Bus' Current Location
  //lat2, long2 == Stop location
  double calculateDistance(lat1, lon1, lat2, lon2){
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
            (1 - c((lon2 - lon1) * p))/2;
    return 12742 * asin(sqrt(a));
  }

}

