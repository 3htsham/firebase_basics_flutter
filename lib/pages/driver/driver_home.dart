import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_pref/model/bus.dart';
import 'package:shared_pref/model/user.dart';
import 'package:shared_pref/pages/auth/login.dart';
import 'package:shared_pref/pages/driver/bus_details.dart';
import '../../global_variables.dart';
import 'notification.dart';

class DriverHome extends StatefulWidget {
  MyUser currentUser;

  DriverHome({Key key, this.currentUser}) : super(key: key);

  @override
  _DriverHomeState createState() => _DriverHomeState();
}

class _DriverHomeState extends State<DriverHome> {
  MyUser currentUser;

  @override
  initState() {
    this.currentUser = widget.currentUser;
    super.initState();
    subscribeNotificationsTopic();
  }

  _logout() async {
    ///Set prefs to loggedOut
    ///Clear the Activity Stack
    ///Navigate to Login page
    //UnSubscribe to FCM Topic
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    try {
      messaging.unsubscribeFromTopic(GlobalVariables.riderNotificationsTopic);
    } catch (e) {

    }

    await FirebaseAuth.instance.signOut();

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
  }

  subscribeNotificationsTopic(){
    //Subscribe to FCM General Topic
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    messaging.subscribeToTopic(GlobalVariables.riderNotificationsTopic);
  }

  Future<List<Bus>> _getMyBuses() async {
    List<Bus> _buses = [];
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference buses = firestore.collection('buses');
    final ref = buses.where('driverId', isEqualTo: this.currentUser.id);
    final snapshot = await ref.get();
    if(snapshot.docs.isNotEmpty) {
      final docs = snapshot.docs;
      docs.forEach((docSnap) {
        final _bus = Bus.fromJson(docSnap.data());
        _buses.add(_bus);
      });
    }
    return _buses;
  }


  @override
  Widget build(BuildContext context) {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;

    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome ${this.currentUser?.name ?? ''}'),
        actions: [
          TextButton(
              onPressed: () {
                _logout();
              },
              child: Text(
                "Logout",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(color: Colors.black),
              ))
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [

            SizedBox(height: 15,),


            FutureBuilder(
              future: this._getMyBuses(),
              builder: (context, AsyncSnapshot<List<Bus>> _snap) {

                final _data = _snap.data;

                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _data?.isEmpty ?? false
                        ? SizedBox()
                        : Text(
                      'Your Buses',
                      style: textTheme.subtitle2,
                    ),
                    SizedBox(height: 15,),
                    _data?.isEmpty ?? false
                        ? SizedBox()
                        : ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: _data?.length ?? 0,
                      itemBuilder: (context, index) {
                        return ListTile(
                          onTap: (){
                            //TODO: Navigate to next Page i.e. Bus Details
                            Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context) => BusDetailsPage(
                                      currentUser: this.currentUser,
                                      bus: _data[index],
                                    )
                                )
                            );
                          },
                          title: Text('Bus No. (${_data[index].busNo})', style: textTheme.bodyText1,),
                          leading: Icon(CupertinoIcons.car_detailed, color: Colors.green,),
                          trailing: IconButton(
                            onPressed: (){
                              //TODO: Navigate to next Page i.e. Bus Details
                              Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (context) => BusDetailsPage(
                                        currentUser: this.currentUser,
                                        bus: _data[index],
                                      )
                                  )
                              );
                            },
                            icon: Icon(CupertinoIcons.chevron_forward, color: Colors.redAccent,),
                          ),
                        );
                      },
                    ),
                  ],
                );
              },
            ),


          ],
        ),
      ),
    );
  }
}
