import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_pref/model/bus.dart';
import 'package:shared_pref/model/bus_route.dart';
import 'package:shared_pref/model/user.dart';
import 'package:shared_pref/pages/driver/location_tracking.dart';

class BusDetailsPage extends StatefulWidget {
  MyUser currentUser;
  Bus bus;

  BusDetailsPage({Key key, this.currentUser, this.bus}) : super(key: key);

  @override
  _BusDetailsPageState createState() => _BusDetailsPageState();
}

class _BusDetailsPageState extends State<BusDetailsPage> {
  MyUser currentUser;
  Bus bus;
  List<BusRoute> routes = [];

  @override
  void initState() {
    this.currentUser = widget.currentUser;
    this.bus = widget.bus;

    super.initState();
  }

  Future<List<BusRoute>> _getBusRoutes() async {
    List<BusRoute> _routes = [];
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference stops = firestore.collection('routes');

    for(int i = 0; i<this.bus.stops.length; i++) {
      final _current = this.bus.stops[i];
      final ref = stops.doc(_current);
      final snapshot = await ref.get();
      if(snapshot.exists) {
        final _route = BusRoute.fromJson(snapshot.data());
        _routes.add(_route);
        this.routes.add(_route);
      }
    }

    return _routes;
  }

  @override
  Widget build(BuildContext context) {
    final theme=  Theme.of(context);
    final textTheme = theme.textTheme;

    return Scaffold(
      appBar: AppBar(
        title: Text('Bus No: ${this.bus?.busNo ?? ''}'),
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon(CupertinoIcons.back, color: Colors.black,),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [



              SizedBox(height: 15,),

              ElevatedButton(
                onPressed: () {
                  if(this.routes != null && this.routes.length > 0) {
                    //TODO: Navigate to location tracking screen
                    Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) =>
                                LocationTrackingPage(
                                  currentUser: this.currentUser,
                                  bus: this.bus,
                                  routes: this.routes,
                                )
                        )
                    );
                  }
                },
                style: ElevatedButton.styleFrom(shape: StadiumBorder()),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Opacity(
                        opacity: 0,
                          child: Icon(CupertinoIcons.location_solid, color: Colors.white,)),
                      Text("Start Location Tracking"),
                      Icon(CupertinoIcons.location_solid, color: Colors.white,)
                    ],
                  ),
                ),
              ),

              SizedBox(height: 15,),

              FutureBuilder(
                future: this._getBusRoutes(),
                builder: (context, AsyncSnapshot<List<BusRoute>> _snap) {

                  final _data = _snap.data;

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _data?.isEmpty ?? false
                          ? SizedBox()
                          : Text(
                        'Routes/Stops Assigned',
                        style: textTheme.subtitle2,
                      ),
                      SizedBox(height: 15,),
                      _data?.isEmpty ?? false
                          ? SizedBox()
                          : ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: _data?.length ?? 0,
                        itemBuilder: (context, index) {
                          return ListTile(
                            title: Text('${_data[index].name}', style: textTheme.bodyText1,),
                            leading: Icon(CupertinoIcons.location, color: Colors.green,),
                          );
                        },
                      )
                    ],
                  );
                },
              ),

            ],
          ),
        ),
      ),
    );
  }
}
