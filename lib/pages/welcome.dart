import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_pref/model/user.dart';
import 'package:shared_pref/pages/chat.dart';
import 'package:shared_pref/pages/auth/login.dart';


class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {

  User currentUser;

  List<Map<String, dynamic>> posts = [];


  TextEditingController searchController = TextEditingController();

  List<MyUser> users = <MyUser>[];

  @override
  initState() {
    currentUser = FirebaseAuth.instance.currentUser;
    super.initState();
    insertValue("Some post title goes here");
  }

  insertValue(String title) {
    Map<String, dynamic> post = Map();
    post['id'] = 1;
    post['userId'] = 1;
    post['title'] = title;
    post['body'] = "lorem ipsum dollor simet lorem ipsum dollor simet lorem ipsum dollor simet lorem ipsum dollor simet";

    setState(() {
      this.posts.add(post);
    });
  }

  _logout() async {
    ///Set prefs to loggedOut
    ///Clear the Activity Stack
    ///Navigate to Login page
    await FirebaseAuth.instance.signOut();

    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
  }



  searchUser() async {
    if(searchController.text != null && searchController.text.contains("@") && searchController.text.contains(".")) {
      setState((){
        this.users.clear();
      });

      FirebaseFirestore firestore = FirebaseFirestore.instance;
      // Create a CollectionReference called users that references the firestore collection
      CollectionReference users = firestore.collection('users');

      users
          .where('email', isEqualTo: searchController.text.toLowerCase())
          .get()
          .then((_snapshot) {
            var documents = _snapshot.docs;
            if(documents.isNotEmpty) {
              var document = documents[0];
              var userData = document.data() as Map;

              MyUser _searchedUser = MyUser();
              _searchedUser.name = userData['name'];
              _searchedUser.id = userData['id'];
              _searchedUser.email = userData['email'];

              ///If searched user is not the current logged in user
              if(FirebaseAuth.instance.currentUser.uid != _searchedUser.id) {
                setState(() {
                  this.users.add(_searchedUser);
                });
                searchController.clear();
              } else {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("No user found")));
              }

            } else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("No user found")));
            }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        title: Text(
            currentUser?.displayName ?? " ",
          style: Theme.of(context).textTheme.subtitle2,
        ),
        leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Colors.white,),
          onPressed: (){
            // Navigator.of(context).pop();
          },
        ),
        actions: [
          TextButton(onPressed: (){
            _logout();
          }, child: Text("Logout", style: Theme.of(context).textTheme.bodyText1,))
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(15),
        child: Center(
          child: Column(
            children: [

              Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                        keyboardType: TextInputType.emailAddress,
                        style: Theme.of(context).textTheme.bodyText1,
                        controller: searchController,
                        decoration: InputDecoration(
                            hintText: "Search by Email",
                            hintStyle: Theme.of(context).textTheme.bodyText1,
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white, width: 1)
                            ),
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: (){
                        searchUser();
                      },
                      child: Text(
                        "Search"
                      ),
                    )
                  ],
                ),
              ),


              this.users.isEmpty
              ? SizedBox()
                  : ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: this.users.length,
                itemBuilder: (context, i) {
                  return ListTile(
                    onTap: (){
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) =>
                              ChatScreen(receiver: this.users[i],)
                          ));
                    },
                    title: Text(this.users[i].name),
                    subtitle: Text(this.users[i].email),
                  );
                },
              )


            ],
          ),
        ),
      ),
    );
  }
}
