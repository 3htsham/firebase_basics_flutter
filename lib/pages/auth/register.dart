import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_pref/database.dart';
import 'package:shared_pref/model/user.dart';
import 'package:shared_pref/pages/auth/login.dart';
import 'package:shared_pref/pages/welcome.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  bool isLoading = false;

  MyUser user = MyUser();

  GlobalKey<FormState> formKey = GlobalKey();


  _validateForm() async {
    if(formKey.currentState.validate()) {
      formKey.currentState.save();

      ///Register user here
      setState(() {
        isLoading = true;
      });

      try {
        UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
            email: this.user.email.toString(),
            password: this.user.password.toString()
        );

        await userCredential.user.updateDisplayName(this.user.name);

        await userCredential.user.sendEmailVerification();

        print("User id is: " + userCredential.user.uid);

        ///Add user to Firestore
        await addUserToFirestore(userCredential.user, this.user.name);

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Check your email for verification")));

        setState(() {
          isLoading = false;
        });

        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => LoginPage()));

      } on FirebaseAuthException catch (e) {
        setState(() {
          isLoading = false;
        });
        if (e.code == 'weak-password') {
          print('The password provided is too weak.');
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("The password provided is too weak.")));
        } else if (e.code == 'email-already-in-use') {
          print('The account already exists for that email.');
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("The account already exists for that email.")));
        }
      } catch (e) {
        setState(() {
          isLoading = false;
        });
        print(e);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Error registering new account")));
      }

    }
  }

  Future<void> addUserToFirestore(User _user, String _updatedName) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    // Create a CollectionReference called users that references the firestore collection
    CollectionReference users = firestore.collection('users');
    this.user.email = _user.email.toLowerCase();
    this.user.id = _user.uid;

    ///For manual Doc Id
    users.doc(_user.uid).set(this.user.toMap())
    .then((value) => print("User Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: Form(
            key: this.formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [

                Icon(CupertinoIcons.star, color: Colors.white, size: 55,),

                SizedBox(height: 30,),

                TextFormField(
                  keyboardType: TextInputType.text,
                  validator: (value) => (value != null && value.length > 1) ? null : "Name can't be empty",
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                      hintText: "Name",
                      hintStyle: Theme.of(context).textTheme.bodyText1,
                      border: InputBorder.none
                  ),
                  onSaved: (value) {
                    user.name = value;
                  },
                ),

                SizedBox(height: 10,),

                TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) => (value != null && value.contains("@") && value.contains(".")) ? null : "Email must be valid",
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                      hintText: "Email",
                      hintStyle: Theme.of(context).textTheme.bodyText1,
                      border: InputBorder.none
                  ),
                  onSaved: (value) {
                    user.email = value;
                  },
                ),

                SizedBox(height: 10,),

                TextFormField(
                  obscureText: true,
                  validator: (value) => (value != null && value.length >= 6) ? null : "Password must contain at least 6 chars",
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                      hintText: "Password",
                      hintStyle: Theme.of(context).textTheme.bodyText1,
                      border: InputBorder.none
                  ),
                  onSaved: (value) {
                    user.password = value;
                  },
                ),

                SizedBox(height: 20,),

                Row(
                  children: [

                    Text("Rider"),

                    Switch(value: this.user.isDriver, onChanged: (value) {
                      setState(() {
                        this.user.isDriver = value;
                      });
                    }),
                    Text("Driver"),
                  ],
                ),

                this.isLoading
                ? SizedBox(
                  height: 35,
                    child: CircularProgressIndicator())
                :
                ElevatedButton(
                  onPressed: (){
                    _validateForm();
                  },
                  style: ElevatedButton.styleFrom(
                      shape: StadiumBorder()
                  ),
                  child: Center(
                    child: Text(
                        "Register"
                    ),
                  ),
                ),

                SizedBox(height: 10,),

                TextButton(onPressed: (){
                  Navigator.of(context).pop();
                },
                    child: Center(
                      child: Text("Back to Login"),
                    ))

              ],
            ),
          ),
        ),
      ),
    );
  }
}
