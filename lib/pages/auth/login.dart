import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_pref/model/user.dart';
import 'package:shared_pref/pages/auth/forgot_password.dart';
import 'package:shared_pref/pages/auth/register.dart';
import 'package:shared_pref/pages/driver/driver_home.dart';
import 'package:shared_pref/pages/main_pages/home.dart';
import 'package:shared_pref/pages/rider/rider_home.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  bool isLoading = false;

  MyUser user = MyUser();

  GlobalKey<FormState> formKey = GlobalKey();


  _validateForm() async {
    if(formKey.currentState.validate()) {
      formKey.currentState.save();
      ///Login user here
      setState(() {
        isLoading = true;
      });

      try {
        UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: this.user.email.toString(),
            password: this.user.password.toString()
        );
        setState(() {
          isLoading = false;
        });

        if(userCredential != null) {
          final user = await _getUserDetails();
          bool isVerified = userCredential.user.emailVerified;
          if(isVerified) {
            //Check if user is not admin
            if(user.type == null && user.type != 'admin') {
              //Check if user is Driver or Rider
              if(user.isDriver) {
                //TODO: Navigate to driver module
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => DriverHome(currentUser: user,)));
              } else {
                //TODO: Navigate to Rider Modules
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => RiderHome(currentUser: user,)));
              }
            } else {
              await FirebaseAuth.instance.signOut();
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("You are not allowed to Login to this app")));
            }
          } else {
            await userCredential.user.sendEmailVerification();
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Check your email for verification")));
            this.formKey.currentState.reset();
          }
        }

      } on FirebaseAuthException catch (e) {
        setState(() {
          isLoading = false;
        });
        if (e.code == 'user-not-found') {
          print('No user found for that email.');
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("No user found for that email.")));
        } else if (e.code == 'wrong-password') {
          setState(() {
            isLoading = false;
          });
          print('Wrong password provided for that user.');
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Wrong password provided for that user.")));
        }
      }
    }
  }

  Future<MyUser> _getUserDetails() async {
    final _authUser = FirebaseAuth.instance.currentUser;
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference users = firestore.collection('users');
    final _userDetails = await users.doc(_authUser.uid).get();
    Map data = _userDetails.data();
    return MyUser.fromJson(data);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: Form(
            key: this.formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [

                Icon(CupertinoIcons.star, color: Colors.white, size: 55,),

                SizedBox(height: 30,),

                TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) => (value != null && value.contains("@") && value.contains(".")) ? null : "Email must be valid",
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                      hintText: "Email",
                      hintStyle: Theme.of(context).textTheme.bodyText1,
                    border: InputBorder.none
                  ),
                  onSaved: (value) {
                    user.email = value;
                  },
                ),

                SizedBox(height: 10,),

                TextFormField(
                  obscureText: true,
                  validator: (value) => (value != null && value.length >= 6) ? null : "Password must contain at least 6 chars",
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                      hintText: "Password",
                    hintStyle: Theme.of(context).textTheme.bodyText1,
                      border: InputBorder.none
                  ),
                  onSaved: (value) {
                    user.password = value;
                  },
                ),

                SizedBox(height: 20,),

                this.isLoading
                    ? SizedBox(height: 35, child: CircularProgressIndicator())
                    : ElevatedButton(
                        onPressed: () {
                          _validateForm();
                        },
                        style: ElevatedButton.styleFrom(shape: StadiumBorder()),
                        child: Center(
                          child: Text("Login"),
                        ),
                      ),


                SizedBox(height: 10,),

                TextButton(onPressed: (){
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => RegisterPage()
                  ));
                },
                    child: Center(
                  child: Text("Signup instead"),
                )),

                TextButton(onPressed: (){
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ForgotPassword()
                  ));
                },
                    child: Center(
                      child: Text("Forgot Password"),
                    )),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
