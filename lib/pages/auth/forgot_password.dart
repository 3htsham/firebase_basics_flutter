import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';


class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {

  TextEditingController emailController = TextEditingController();

  bool isLoading = false;


  _validateAndProceed() async {
    var value = emailController.text;
    if(value != null && value.contains("@") && value.contains(".")) {
      setState(() {
        isLoading = true;
      });

      try {

        await FirebaseAuth.instance.sendPasswordResetEmail(email: value);

        emailController.clear();

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("A reset password link sent to you email.")));

        Navigator.of(context).pop();

        setState(() {
          isLoading = false;
        });

      } on FirebaseAuthException catch (e) {
        setState(() {
          isLoading = false;
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.message)));
      }
      catch (e) {
        setState(() {
          isLoading = false;
        });
        print(e);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Error sending email.")));
      }


    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [

              TextField(
                keyboardType: TextInputType.emailAddress,
                controller: emailController,
                // validator: (value) => (value != null && value.contains("@") && value.contains(".")) ? null : "Email must be valid",
                style: Theme.of(context).textTheme.bodyText1,
                decoration: InputDecoration(
                    hintText: "Email",
                    hintStyle: Theme.of(context).textTheme.bodyText1,
                    border: InputBorder.none
                ),
              ),

              SizedBox(height: 20,),

              this.isLoading
                  ? SizedBox(height: 35, child: CircularProgressIndicator())
                  : ElevatedButton(
                onPressed: () {
                  _validateAndProceed();
                },
                style: ElevatedButton.styleFrom(shape: StadiumBorder()),
                child: Center(
                  child: Text("Send Reset Password Link"),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
