
class DistanceModel {
  String routeId;
  double distance;
  String routeName;
  DistanceModel({this.routeId, this.distance, this.routeName});
}