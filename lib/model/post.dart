class Post {
  var userId;
  var id;
  var title;
  var body;

  Post({
    this.title, this.id, this.body, this.userId
  });

  Post.convertFromJson(Map<String, dynamic> data) {
    this.id = data['id'];
    this.title = data['title'];
    this.userId = data['userId'];
    this.body = data['body'];
  }


  Map<String, dynamic> convertToJson() {
    Map<String, dynamic> post = Map();
    post['id'] = this.id;
    post['userId'] = this.userId;
    post['title'] = this.title;
    post['body'] = this.body;
    return post;
  }

}