class ChatMessage {


  var id;
  var message;
  var time;
  var senderID;

  ChatMessage({this.id, this.message, this.time});


  Map<String, dynamic> toJson(){
    Map<String, dynamic> data = Map();
    data['message'] = this.message;
    data['time'] = this.time;
    data['senderID'] = this.senderID;
    return data;
  }

}