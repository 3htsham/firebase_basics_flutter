class MyUser {

  var id;
  var name;
  var email;
  var password;
  bool isDriver = false;
  var type;
  var stop;

  MyUser({
    this.id, this.password, this.email, this.name, this.isDriver = false,
  });

  MyUser.fromJson(Map<String, dynamic> data) {
    this.id = data['id'];
    this.name = data['name'];
    this.email = data['email'];
    this.isDriver = data['isDriver'];
    this.type = data['type'] != null ? data['type'] : null;
    this.stop = data['stop'] != null ? data['stop'] : null;
  }

  Map<String, dynamic> toMap(){
    Map<String, dynamic> data = Map();
    data['name'] = this.name;
    data['email'] = this.email;
    data['id'] = this.id;
    data['isDriver'] = this.isDriver;
    return data;
  }

}