import 'package:cloud_firestore/cloud_firestore.dart';

class BusRoute {

  var id;
  var name;

  LocationModel location;

  BusRoute();

  BusRoute.fromJson(Map<String, dynamic> data){
    this.id = data['id'];
    this.name = data['name'];
    if(data['location'] != null) {
      final _routeLocation = data['location'] as GeoPoint;
      final model = LocationModel(latitude: _routeLocation.latitude, longitude: _routeLocation.longitude);
      this.location = model;
    }
  }

  Map<String, dynamic> toMap(){
    Map<String, dynamic> data = Map();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }

}

class LocationModel {
  var latitude;
  var longitude;

  LocationModel({this.latitude, this.longitude});

}